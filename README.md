# README #

Working code for NodeMCU ESP8266 devkit

### What is this repository for? ###

* Quick summary
This code is for public consumption - a working example of using NodeMCU webserver to remotely control the pins on the devkit

* Version CT12

### How do I get set up? ###

* Summary of set up
1. ESP8266 devkit flashed with NodeMCU
2. A LUA Uploader
3. Edit set_wifi.lua to reflect your wifi SSID and password, plus your chosen TCP port number
4. Copy set_wifi.lua to the devkit
5. Copy init.lua to the devkit


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
Charles Thompson
haryadoon@gmail.com

* Other community or team contact