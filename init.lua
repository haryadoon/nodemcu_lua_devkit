require "set_wifi";
http_port = set_wifi();

vers = "CT12";

pins = {};
pins[1] = 1;
pins[2] = 2;
pins[3] = 3;
pins[4] = 4; 
pins[5] = 0;
pins[6] = 5;
pins[7] = 6;
pins[8] = 7;

for i = 1, 8 do
	gpio.mode(pins[i], gpio.OUTPUT);
end;

pin_off = gpio.HIGH;
pin_on = gpio.LOW;

-- the following lines could be replaced with a set of images named appropriately and loaded in a for loop.
--img_on  = "https://dl.dropboxusercontent.com/u/75349848/NodeMCU/led4_on.jpg";
--img_off = "https://dl.dropboxusercontent.com/u/75349848/NodeMCU/led4_off.jpg";

-- on/off images for each pin
off_images = {};
on_images = {};
for i = 1, 8 do
	off_images[i] = "https://dl.dropboxusercontent.com/u/75349848/NodeMCU/off" .. i .. ".png";
	on_images[i] = "https://dl.dropboxusercontent.com/u/75349848/NodeMCU/on" .. i .. ".png";
end;

if srv then srv:close() srv=nil end
srv = net.createServer(net.TCP)
srv:listen(http_port, function(conn)
	conn:on("receive", function(sck, req)
	
		-- get the HTTP request data
		local _, _, method, path, vars = string.find(req, "([A-Z]+) (.+)?(.+) HTTP");
		if (method == nil) then
			_, _, method, path = string.find(req, "([A-Z]+) (.+) HTTP");
		end;
		local _GET = {};
		if (vars ~= nil) then
			for k, v in string.gmatch(vars, "(%w+)=(%w+)&*") do
				_GET[k] = v;
			end;
		end;
		
		-- set NodeMCU pins based on what we were asked to do in the request
		for i = 1, 8 do
			if    (_GET.pin == "ON"..i) then gpio.write(pins[i], pin_on);
			elseif(_GET.pin == "OFF"..i)then gpio.write(pins[i],pin_off);
			end;
		end;

		if(_GET.pin == "ONALL")then
			for i = 1, 8 do 
				gpio.write(i, pin_on);
			end;
		elseif(_GET.pin == "OFFALL")then
			for i = 1, 8 do 
				gpio.write(i, pin_off);
			end;
		end;

		-- what state is each of the pins in?
		local pin_state = {}; -- empty array
		for i = 1, 8 do
			pin_state[i] = gpio.read(pins[i]); -- current state is saved in pin_state
		end;

		-- declare some arrays to hold the text values to be used in the following HTML
		local btn_data = {};
		local btn_url = {};
		local btn_url_new = {};
		
		for i = 1, 8 do
			if (pin_state[i] == pin_off) then
				btn_data[i] = "ON"..i;
				-- btn_url[i] = img_off;
				-- btn_url_new[i] = img_on;
				btn_url[i] = off_images[i];
				btn_url_new[i] = on_images[i];
			elseif (pin_state[i] == pin_on) then
				btn_data[i] = "OFF"..i;
				-- btn_url[i] = img_on;
				-- btn_url_new[i] = img_off;
				btn_url[i] = on_images[i];
				btn_url_new[i] = off_images[i];
			end;
		end;
		-- now we have a set of arrays with appropriate text in them, to be used in the HTML code below

		-- use an array (response) to hold each line of HTML code as we build it up
		local response = {"HTTP/1.0 200 OK\r\nServer: NodeMCU on ESP8266\r\nContent-Type: text/html\r\n\r\n"};
		response[#response + 1] = "<html>\r\n";
		response[#response + 1] = "  <head>\r\n";
		response[#response + 1] = "    <link rel='icon' href='http://stjernetegn.com/favicon.ico' type='image/x-icon'>\r\n";
		response[#response + 1] = "    <style>table, td, button {font-size:30px;}</style>\r\n";
		response[#response + 1] = "    <title>ESP8266 Control (V"..vers..")</title>\r\n";
		response[#response + 1] = "  </head>\r\n";
		response[#response + 1] = "  <body background='https://s-media-cache-ak0.pinimg.com/736x/0d/b0/29/0db0297b8da3af7e3f73909cd9e5e0b3.jpg'>\r\n";
		response[#response + 1] = "    <font face='Arial' size='7'>\r\n";
		response[#response + 1] = "    <h1><center>ESP8266 Control (v"..vers..")</center></h1>\r\n";
		response[#response + 1] = "    <center><table cellpadding='20px'>\r\n";
		
		-- use a for loop to build a table row for each relay from 1 to 8
		for i = 1, 8, 2 do -- each row in our HTML table will have two pins showing
			response[#response + 1] = "      <tr>\r\n"; -- begin a row (it will have four columns)
			response[#response + 1] = "        <td>Relay "..i.."</td>\r\n"; -- column 1 in the row
			response[#response + 1] = "        <td>\r\n"; -- column 2
			response[#response + 1] = "          <a href='?pin="..btn_data[i].."'>\r\n";
			response[#response + 1] = "            <input type='image' \r\n";
			response[#response + 1] = "                   src='"..btn_url[i].."' \r\n";
			response[#response + 1] = "                   onclick='this.src=\""..btn_url_new[i].."\";' />\r\n";
			response[#response + 1] = "          </a>\r\n";
			response[#response + 1] = "        </td>\r\n"; -- end of column 2
			response[#response + 1] = "        <td>Relay ".. (i+1) .."</td>\r\n"; -- column 3 in the row
			response[#response + 1] = "        <td>\r\n"; -- column 4
			response[#response + 1] = "          <a href='?pin="..btn_data[i+1].."'>\r\n";
			response[#response + 1] = "            <input type='image' \r\n";
			response[#response + 1] = "                   src='"..btn_url[i+1].."' \r\n";
			response[#response + 1] = "                   onclick='this.src=\""..btn_url_new[i+1].."\";' />\r\n";
			response[#response + 1] = "          </a>\r\n";
			response[#response + 1] = "        </td>\r\n"; -- end of column 4
			response[#response + 1] = "      </tr>\r\n"; -- end of the row
		end;
		
		response[#response + 1] = "    </table></center>\r\n";
		response[#response + 1] = "    <p><center>All Relays \r\n";
		response[#response + 1] = "      <a href=\"?pin=ONALL\"><button>ON</button></a>&nbsp;\r\n";
		response[#response + 1] = "      <a href=\"?pin=OFFALL\"><button>OFF</button></a>\r\n";
		response[#response + 1] = "    </center></p>\r\n";
		response[#response + 1] = "    </font>\r\n";
		response[#response + 1] = "  </body>\r\n";
		response[#response + 1] = "</html>\r\n";
		
		 -- sends and removes the first element from the 'response' table
		local function send(sk)
			local buf = "";
			if #response > 0 then
				for i = 1, 30 do
					if #response > 0 then
						buf = buf .. table.remove(response, 1);
					end;
				end;
				sk:send(buf);
				buf = "";
			else
				sk:close()
				response = nil
			end
		end

		-- triggers the send() function again once the first chunk of data was sent
		sck:on("sent", send);
		send(sck);
		
	end)
end)